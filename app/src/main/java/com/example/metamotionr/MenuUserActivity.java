package com.example.metamotionr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.multidex.MultiDex;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import java.time.LocalDate;
import java.util.Date;

/**
 *Ta klasa zawiera kalendarz i metodę nasłuchującą jego zmianę. Jeśli użytkownik wybierze datę i kliknie przycisk, data jest przesyłana do aktywności AnalyzeActivity i następnie rozpoczyna jej działanie
 */
public class MenuUserActivity extends AppCompatActivity {

    private CalendarView calendarView;
    private LocalDate selectedDate;

    @Override
    protected void attachBaseContext(Context base){
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_user);

        if (MeasurementActivity.valuesX !=null){
        }

        calendarView = findViewById(R.id.calendar_view);
        calendarView.setMaxDate(new Date().getTime());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                selectedDate = LocalDate.of(year, month+1, dayOfMonth);
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClickSearch(View view)
    {
        Intent intent =new Intent(this, AnalyzeActivity.class);
        intent.putExtra("selectedDate",String.valueOf(selectedDate));
        this.startActivity(intent);
    }

}
