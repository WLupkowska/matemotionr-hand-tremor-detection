package com.example.metamotionr;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 *Klasa odpowiedzialna za funkcjonalności logowania, rejestracji. W panelu dostępne są 2 przyciski dla użytkownika: „LOGIN”, „REGISTER”. Po poprawnym zalogowaniu użytkownik widzi dymek (toast) z informacją „Authentication OK”, następnie automatycznie jest przenoszony do następnego okna związanego z UserActivity.
 */
public class LoginActivity extends AppCompatActivity
{

    private static final String TAG = "IBActivity";
    private FirebaseAuth mAuth;
    public Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        btnSignIn = (Button)findViewById(R.id.btnWprowadzanie);

    }

    public void onSignIn(View view)
    {
        EditText edtEmail = (EditText)findViewById(R.id.editDane1);
        EditText edtPassword = (EditText)findViewById(R.id.editDane2);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(this,"Please fill all fields",Toast.LENGTH_SHORT).show();
        }else {

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, (task) -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Toast.makeText(getApplicationContext(), "Authentication OK",
                                    Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(this, UserActivity.class);
                            this.startActivity(intent);

                        } else {
                            Log.d(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }



    public void onSignUp(View view) {
        EditText edtEmail = (EditText) findViewById(R.id.editDane1);
        EditText edtPassword = (EditText) findViewById(R.id.editDane2);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }else if (password.length()<7) {
            Toast.makeText(this,"Your password is too short (min.6 characters)",Toast.LENGTH_SHORT).show();
        }else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "createUserWithEmail:success");
                                Toast.makeText(getApplicationContext(), "Account created",
                                        Toast.LENGTH_LONG).show();
                                FirebaseUser user = mAuth.getCurrentUser();

                            } else {
                                Log.w(TAG, "CreateUserWithEmail: failure", task.getException());
                                Toast.makeText(getApplicationContext(), "Account was not created",
                                        Toast.LENGTH_LONG).show();
                            }
                        }

                    });

        }
    }

}//class
