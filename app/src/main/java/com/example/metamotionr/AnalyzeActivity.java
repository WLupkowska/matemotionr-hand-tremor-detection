package com.example.metamotionr;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 *
 */
public class AnalyzeActivity extends AppCompatActivity {

    private static final String TAG = "IBActivity";
    private FirebaseFirestore nFirestore = FirebaseFirestore.getInstance();
    private FirebaseAuth afFirestore = FirebaseAuth.getInstance();
    private String userID = Objects.requireNonNull(afFirestore.getCurrentUser()).getUid();
    private Spinner spinner;
    private ArrayList<Measurment> measurmentsList = new ArrayList<Measurment>();
    private TextView textViewAnaliza;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analyze);
        String date = getIntent().getStringExtra("selectedDate");
        spinner = findViewById(R.id.dateSpinner);
        ArrayList<String> hours = new ArrayList<String>();
        ArrayAdapter<String> spinnerItems = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, hours);
        spinnerItems.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        CollectionReference measurements = nFirestore.collection("USERS")
                .document(userID).collection("Measurments");
        measurements.whereEqualTo("date", date)
                .addSnapshotListener(this, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots.isEmpty()) {
                            Toast.makeText(AnalyzeActivity.this, "No hours, change the date.", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                            return;
                        }
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            Measurment measurment = documentSnapshot.toObject(Measurment.class);
                            measurment.setMeasurmentId(documentSnapshot.getId());
                            Measurment measurment1 = new Measurment();
                            measurment1.setMeasurmentId(measurment.getMeasurmentId());
                            measurment1.setDate(measurment.getDate());
                            String hour = measurment.getHour();
                            measurment1.setHour(hour);
                            spinnerItems.add(hour);
                            measurment1.setAxisX(measurment.getAxisX());
                            measurment1.setAxisY(measurment.getAxisY());
                            measurment1.setAxisZ(measurment.getAxisZ());
                            measurmentsList.add(measurment1);
                        }
                        spinner.setAdapter(spinnerItems);
                        Toast.makeText(AnalyzeActivity.this, "Done",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void onClickAnalyze(View view) {
        String selectedHour = (String) spinner.getSelectedItem();
        for (Measurment measurment : measurmentsList) {
            if (measurment.getHour().equals(selectedHour)) {
                MeasurmentAnalysis measurmentAnalysis = new MeasurmentAnalysis();
                measurmentAnalysis.setInfo(25, measurment.getAxisY(), measurment.getAxisX());
                textViewAnaliza = findViewById(R.id.textViewAnaliza);

                textViewAnaliza.setText("Distance: "+measurmentAnalysis.getDistance()+" [m] \n average velocity: "+measurmentAnalysis.getAverageVelocity()+" [m/s] \n steps: "+measurmentAnalysis.getNumberOfSteps());


            }
        }
    }

}
