package com.example.metamotionr;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa odpowiedzialna za analizowanie i wyznaczanie parametrów chodu
 */
public class MeasurmentAnalysis {


    private int numberOfSteps = 0;
    private double distance = 0;
    private double averageVelocity = 0;


    /**
     * Głowna klasa analizująca chód i ustawiająca głowne parametry
     * @param frequency z jaką notowane są kolejne punkty pomiarowe
     * @param axisX dane z osi X akcelerometra
     * @param axisY dane z osi Y akcelerometra
     */
    void setInfo(double frequency, List<Double> axisX, List<Double> axisY) {
        ArrayList<Step> steps = new ArrayList<>();
        System.out.println(axisX.get(1) + " y: " + axisY.get(1));
        // lista z punktami, mamy x i y juz z przyspieszeniem ziemskim i x jest od zera
        for (int i = 0; i < axisX.size(); i++) {
            steps.add(new Step(i, (axisX.get(i) - axisX.get(0)) * 9.81, axisY.get(i) * 9.81, 0.04 * i));
        }
        //usuwanie ostatnich pomiarów
        int index = steps.size() - 1;
        double lastY = steps.get(steps.size() - 1).y;
        double lastX = steps.get(steps.size() - 1).x;
        boolean loop = true;
        while (loop) {
            if (steps.get(index).y < lastY + 0.2 && steps.get(index).y > lastY - 0.2 && steps.get(index).x < lastX + 0.2 && steps.get(index).x > lastX - 0.2) {
                steps.remove(index);
                index--;
            } else
                loop = false;
        }
        // wyliczanie srednich i wartosci min X
        double avgX = 0; // srednia X
        double avgY = 0; // srednia Y
        double minX = 0; // min X
        for (int i = 1; i < steps.size(); i++) {
            avgX += steps.get(i).x / (steps.size());
            avgY += steps.get(i).y / (steps.size());
            if (steps.get(i).x < minX)
                minX = steps.get(i).x;
        }
        System.out.println(avgY + "minx: " + minX);

        // przekształcanie danych na wektor i odejmowanie wektora g w celu otrzymania przyspieszenia
        for (int i = 0; i < steps.size(); i++) {
            double s2 = Math.pow(steps.get(i).x, 2) + Math.pow(steps.get(i).y, 2);
            double a2 = s2 - Math.pow(steps.get(0).y, 2);
            double aNoSign = Math.sqrt(Math.abs(a2));
            //System.out.println(s2+" a2 "+a2+" aNoS "+aNoSign);
            if (steps.get(i).x < 0 || steps.get(i).y < 0.7 * avgY) {
                double multiplier = 1;
                if (steps.get(i).x > 0.5 * minX) {
                    //multiplier = 1 / 2 * Math.log((steps.get(i).x * (-1)) + 7);
                    System.out.println("mnoznik > -4.6: " + multiplier);
                } else {
                    //multiplier = 3 / 4 * Math.log((steps.get(i).x * (-1)) - 3) + 1;
                    multiplier = 2;
                    System.out.println("mnoznik < -4.6: " + multiplier);
                }
                steps.get(i).setA(aNoSign * (-1) * multiplier);
                steps.get(i).setV(steps.get(i).a * 0.04);
            } else {
                steps.get(i).setA(aNoSign);
                steps.get(i).setV(steps.get(i).a * 0.04);
            }
        }


        double dt = 1 / frequency;
        double time = axisX.size() * dt;
        double avgV = 0; // srednia predkosc
        ArrayList<Double> v = new ArrayList<>();
        v.add(steps.get(0).a * dt);
        for (int i = 1; i < steps.size(); i++) {
            v.add(steps.get(i).a * dt + v.get(i - 1));
            System.out.println(steps.get(i).toString());
            avgV += v.get(i) / (steps.size());
        }
        double dist = 0;
        System.out.println(steps.get(1).toString());
        dist = avgV * steps.get(steps.size() - 1).t;
        System.out.println(avgV + "droga:" + dist);
        setAverageVelocity(avgV);
        setDistance(dist);
        // jest predkosc i droga

        int numberOfSteps = 0;
        ArrayList<Integer> yLess7 = new ArrayList<>();
        ArrayList<Integer> yLess8_5 = new ArrayList<>();
        ArrayList<Integer> yMore10_5 = new ArrayList<>();

        for (int i = 0; i < steps.size(); i++) {
            if (steps.get(i).y < 0.7 * avgY)
                yLess7.add(steps.get(i).nr);
            if (steps.get(i).y < 0.85 * avgY)
                yLess8_5.add(steps.get(i).nr);
            if (steps.get(i).y > 1.15 * avgY)
                yMore10_5.add(steps.get(i).nr);
        }
        // kroki pewne to te y<0.7*avgY
        int nr = 0;
        if (yLess7.size() > 0) {
            nr = yLess7.get(0);
            numberOfSteps++;
            for (int i = 1; i < yLess7.size(); i++) {
                int nrNew = yLess7.get(i);
                if (nrNew - nr > 25) {
                    numberOfSteps++;
                    nr = nrNew;
                }
            }
        }
        System.out.println(numberOfSteps);
        setNumberOfSteps(numberOfSteps * 2 + 1);


    }//end setinfo

    public int getNumberOfSteps() {
        return numberOfSteps;
    }

    public void setNumberOfSteps(int numberOfSteps) {
        this.numberOfSteps = numberOfSteps;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getAverageVelocity() {
        return averageVelocity;
    }

    public void setAverageVelocity(double averageVelocity) {
        this.averageVelocity = averageVelocity;
    }

    @Override
    public String toString() {
        return "MeasurmentAnalysis{" +
                "numberOfSteps=" + numberOfSteps +
                ", distance=" + distance +
                " m , averageVelocity=" + averageVelocity + " m/s " +
                '}';
    }

    /**
     * prywatna klasa przechowująca ważniejsze informacje o punktach pomiarowych
     */
    private class Step {
        private int nr;
        private double x;
        private double y;
        private double v;
        private double a;
        private double t;

        public Step(int nr, double x, double y, double t) {
            this.nr = nr;
            this.x = x;
            this.y = y;
            this.t = t;
        }

        public int getNr() {
            return nr;
        }

        public void setNr(int nr) {
            this.nr = nr;
        }

        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        public double getY() {
            return y;
        }

        public void setY(double y) {
            this.y = y;
        }

        public double getV() {
            return v;
        }

        public void setV(double v) {
            this.v = v;
        }

        public double getT() {
            return t;
        }

        public void setT(double t) {
            this.t = t;
        }

        public double getA() {
            return a;
        }

        public void setA(double a) {
            this.a = a;
        }

        @Override
        public String toString() {
            return "Step{" +
                    "nr=" + nr +
                    ", x=" + x +
                    ", y=" + y +
                    ", v=" + v +
                    ", a=" + a +
                    ", t=" + t +
                    '}';
        }
    }
}
