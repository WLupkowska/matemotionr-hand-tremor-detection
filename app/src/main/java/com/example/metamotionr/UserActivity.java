package com.example.metamotionr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * Powitanie w aplikacji, oferowane 2 możliwości wyboru: zrobienie nowego pomiaru lub analiza jednego z wcześniejszych. Możliwe przekierowania do okien związanych z klasami MeasurementActivity lub MenuUserActivity.
 */
public class UserActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
    }

    /**
     * przenosi do MeasurementActivity
     * @param view
     */
    public void onClickBtnMeasure(View view) {
        Intent intent = new Intent(this, MeasurementActivity.class);
        this.startActivity(intent);
    }

    /**
     * przenosi do MenuUserActivity
     * @param view
     */
    public void onClickBtnSelection(View view) {
        Intent intent = new Intent(this, MenuUserActivity.class);
        this.startActivity(intent);
    }

}
