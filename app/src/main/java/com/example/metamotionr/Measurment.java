package com.example.metamotionr;



import com.google.firebase.firestore.Exclude;

import java.util.List;

/**
 * Klasa modelująca pomiar
 */
public class Measurment
{
    private String measurmentId;
    private String date;
    private String hour;
    private List<Double> axisX;
    private List<Double> axisY;
    private List<Double> axisZ;

    public Measurment() {
    }

    public Measurment(String date, String hour, List<Double> axisX, List<Double> axisY, List<Double> axisZ) {
        this.date = date;
        this.hour = hour;
        this.axisX = axisX;
        this.axisY = axisY;
        this.axisZ = axisZ;
    }

    @Exclude
    public String getMeasurmentId() {
        return measurmentId;
    }

    public void setMeasurmentId(String measurmentId) {
        this.measurmentId = measurmentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public List<Double> getAxisX() {
        return axisX;
    }

    public void setAxisX(List<Double> axisX) {
        this.axisX = axisX;
    }

    public List<Double> getAxisY() {
        return axisY;
    }

    public void setAxisY(List<Double> axisY) {
        this.axisY = axisY;
    }

    public List<Double> getAxisZ() {
        return axisZ;
    }

    public void setAxisZ(List<Double> axisZ) {
        this.axisZ = axisZ;
    }

    @Override
    public String toString() {
        return "Measurment{" +
                "measurmentId='" + measurmentId + '\'' +
                ", date='" + date + '\'' +
                ", hour='" + hour + '\'' +
                ", axisX=" + axisX +
                ", axisY=" + axisY +
                ", axisZ=" + axisZ +
                '}';
    }
}
