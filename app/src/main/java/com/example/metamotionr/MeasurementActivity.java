package com.example.metamotionr;

import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbientlab.metawear.Data;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.Route;
import com.mbientlab.metawear.Subscriber;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.builder.RouteBuilder;
import com.mbientlab.metawear.builder.RouteComponent;
import com.mbientlab.metawear.data.Acceleration;
import com.mbientlab.metawear.module.Accelerometer;

import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import bolts.Continuation;
import bolts.Task;

/**
 * Klasa nawiązująca połączenie z czujnikiem, użycie API. Użytkownik wpisuje czas badania, rozpoczyna je przyciskiem „START”, po zakończeniu pojawia się informacja „finished”, dostępne przyciski „NEXT”,”BACK”
 */
public class MeasurementActivity extends Activity implements ServiceConnection {

    private static final String TAG = "IBActivity";
    static LocalDateTime localDateTime;
    static List<Double> valuesX ;
    static List<Double> valuesY ;
    static List<Double> valuesZ ;
    private BtleService.LocalBinder serviceBinder;
    private MetaWearBoard board;
    private final String MW_MAC_ADDRESS= "C5:6F:20:0C:40:27";
    private Accelerometer accelerometer;
    private FirebaseFirestore nFirestore;
    private FirebaseAuth afFirestore;
    private String userID;

    @Override
    protected void attachBaseContext(Context base){
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * pobranie aktualnej daty, pobranie wpisanego przez użytkownika czasu pomiaru, zapewnienie zbierania danych przez określony czas
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ///< Bind the service when the activity is created
        getApplicationContext().bindService(new Intent(this, BtleService.class),
                this, Context.BIND_AUTO_CREATE);

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                Log.i("metamotion","onclick start");
                localDateTime = LocalDateTime.now();


                EditText etTime = (EditText) findViewById(R.id.timeEditText);
                String timeStr = etTime.getText().toString();
                int time = Integer.valueOf(timeStr);

                accelerometer.acceleration().start();
                accelerometer.start();
                try {
                    TimeUnit.SECONDS.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                accelerometer.stop();
                TextView tvInfo = (TextView) findViewById(R.id.textViewInfo);
                tvInfo.setText("finished");

                System.out.println(valuesX);
                System.out.println(valuesY);
                System.out.println(valuesZ);


            }

        });

    }

    /**
     * zapis danych do bazy, powodzenie procesu sygnalizowane dymkiem” Add data to database:success”
     * @param view
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClickBtnSave(View view) {
        nFirestore = FirebaseFirestore.getInstance();
        afFirestore = FirebaseAuth.getInstance();
        userID = Objects.requireNonNull(afFirestore.getCurrentUser().getUid());
        String time =  localDateTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        CollectionReference measurments = nFirestore.collection("USERS").document(userID).collection("Measurments");
        Measurment measurment = new Measurment(String.valueOf(localDateTime.toLocalDate()),time,valuesX,valuesY,valuesZ);
        measurments.add(measurment).addOnSuccessListener(this, new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(getApplicationContext(), "Add data to database:success",
                        Toast.LENGTH_LONG).show();
            }
        });
        Intent intent = new Intent(this, MenuUserActivity.class);
        this.startActivity(intent);
    }

    public void onClickBtnBack(View view) { //otwiera MainActivityTest
        Intent intent = new Intent(this, UserActivity.class);
        this.startActivity(intent);
    }

    /**
     * rozwiązanie połączenia
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        ///< Unbind the service when the activity is destroyed
        getApplicationContext().unbindService(this);
    }

    /**
     * połączenie z czujnikiem poprzez adres MAC i funkcję retrieveBoard()
     * @param name
     * @param service
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

        ///< Typecast the binder to the service's LocalBinder class
        serviceBinder = (BtleService.LocalBinder) service;
        Log.i("metamotion","Service connected");
        retrieveBoard(MW_MAC_ADDRESS);  //moj komputer: F8:94:C2:F0:AA:91   //metawear : c5:6f:20:0c:40:27  //metawear bluetooth adress c56f200c4027
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
    }

    /**
     * utworzenie obiektu płyty MetaWearBoard w celu połączenia poprzez Bluetooth, jeśli połączenie zostanie nawiązane, zbierane są dane i dodawane do tablic.
     * @param macAddr
     */
    private void retrieveBoard(String macAddr) {

        valuesX = new ArrayList<>();
        valuesY = new ArrayList<>();
        valuesZ = new ArrayList<>();

        final BluetoothManager btManager=
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        final BluetoothDevice remoteDevice=
                btManager.getAdapter().getRemoteDevice(macAddr);

        // Create a MetaWear board object for the Bluetooth Device
        board= serviceBinder.getMetaWearBoard(remoteDevice);
        board.connectAsync().onSuccessTask(new Continuation<Void, Task<Route>>() {
            @Override
            public Task<Route> then(Task<Void> task) throws Exception {

                Log.i("metamotion", "Connected to "+macAddr);
                accelerometer= board.getModule(Accelerometer.class);
                accelerometer.configure()
                        .odr(25f)       // Set sampling frequency to 25Hz
                        .commit();
                return accelerometer.acceleration().addRouteAsync(new RouteBuilder() {
                    @Override
                    public void configure(RouteComponent source) {
                        source.stream(new Subscriber() {
                            @Override
                            public void apply(Data data, Object... env) {

                                Log.i("MainActivity", String.valueOf(data.value(Acceleration.class).x()));
                                valuesX.add((double)data.value(Acceleration.class).x());
                                valuesY.add((double)data.value(Acceleration.class).y());
                                valuesZ.add((double)data.value(Acceleration.class).z());
                            }
                        });
                    }
                });
            }

        }).continueWith(new Continuation<Route, Void>() {
            @Override
            public Void then(Task<Route> task) throws Exception {
                if (task.isFaulted()){
                    Log.w("mbientlab","Failed to configure app", task.getError());

                }else{
                    Log.i("mbientlab", "App configured");
                }

                return null;
            }
        });
    }
}
